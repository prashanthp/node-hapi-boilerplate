import Plugin = require('../amma-plugin-loader/index');

let pkg = require('./package.json');
let PluginLoader = Plugin.default;
let config: Plugin.IConfig = {
  services: {
    'articleService': require('./services/article').default,
    'articleController': require('./controllers/article').default
  },
  routes: require('./routes').default,
  attributes: {
    pkg: pkg
  },
  runs: []
};
let plugin = new PluginLoader(pkg.name, config);
export = plugin;
