import Hapi = require('hapi');
import Code = require('code');
import Lab = require('lab');
import Sinon = require('sinon');
import ArticleController = require('../../../controllers/article');
import ArticleService = require('../../../services/article');
import Mongoose = require('mongoose');
import Boom = require('boom');

let lab = exports.lab = Lab.script(),
  before = lab.before,
  beforeEach = lab.beforeEach,
  afterEach = lab.afterEach,
  after = lab.after,
  expect = Code.expect,
  suite = lab.suite,
  test = lab.test;

suite('Test Article Controller', () => {
  let server;
  let articleService;
  let articleController;
  before((next) => {
    server = new Hapi.Server();
    articleService = new ArticleService.default(server);
    articleController = new ArticleController.default(server);
    server.plugins = {
        'amma-article':{
          articleService: articleService
        }
    };
    next();
  });
  test('get service', (next)=> {
        let service = articleController.getService();
        expect(service).to.equal(articleService);
        return next();
    });

    suite('get all', ()=> {
        test('on success', (next)=> {
            let request = Sinon.spy();
            let response = Sinon.spy();
            let stub = Sinon.stub(articleService, 'findAll', (options, callback) => {
                return callback(null, {});
            });
            articleController.getAll(request, response);
            expect(response.called).to.be.true();
            stub.restore();
            return next();
        });
        test('on failure', (next)=> {
            let request = Sinon.spy();
            let response = Sinon.spy();
            let stub = Sinon.stub(articleService, 'findAll', (options, callback) => {
                return callback('error', {});
            });
            articleController.getAll(request, response);
            expect(response.called).to.be.true();
            expect(response.calledWith(Boom.badImplementation('error'))).to.be.true();
            stub.restore();
            return next();
        });

    });

    suite('get', ()=> {
        let request, response;
        beforeEach((next)=> {
            request = {
                params: {
                    id: 'test'
                }
            };
            response = Sinon.spy();
            return next();

        });
        test('on success', (next)=> {
            let stub = Sinon.stub(articleService, 'findById', (options, callback) => {
                return callback(null, {});
            });
            articleController.get(request, response);
            expect(response.called).to.be.true();
            stub.restore();
            return next();
        });
        test('on not found', (next)=> {
            let stub = Sinon.stub(articleService, 'findById', (options, callback) => {
                return callback(null, null);
            });
            articleController.get(request, response);
            expect(response.called).to.be.true();
            expect(response.calledWith(Boom.notFound('not found'))).to.be.true();
            stub.restore();
            return next();
        });

        test('on failure', (next)=> {
            let stub = Sinon.stub(articleService, 'findById', (options, callback) => {
                return callback('error', {});
            });
            articleController.get(request, response);
            expect(response.called).to.be.true();
            expect(response.calledWith(Boom.badImplementation('error'))).to.be.true();
            stub.restore();
            return next();
        });
    });
    suite('create', ()=> {
        let request, response;
        beforeEach((next)=> {
            request = {
                payload: {
                    _id: 'test'
                }
            };
            return next();
        });
        test('on success', (next)=> {
            let stub = Sinon.stub(articleService, 'create', (options, callback) => {
                return callback(null, {});
            });
            let stub2 = Sinon.stub(articleService, 'findById', (options, callback) => {
                return callback(null, {});
            });

            let createdSpy = Sinon.spy(() => {
                expect(response.called).to.be.true();
                expect(createdSpy.called).to.be.true();
                stub.restore();
                stub2.restore();
                next();
            });
            response = Sinon.spy(() => {
                return {
                    created: createdSpy
                };
            });
            articleController.create(request, response);
        });
        test('on forbidden error', (next)=> {
            let stub = Sinon.stub(articleService, 'create', (options, callback) => {
                return callback('forbidden');
            });
            response = Sinon.spy(() => {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.forbidden('forbidden'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.create(request, response);
        });
        test('on error code 11000', (next)=> {
            let stub = Sinon.stub(articleService, 'create', (options, callback) => {
                return callback({code: 11000});
            });
            response = Sinon.spy(() => {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.forbidden('id exists already'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.create(request, response);
        });
        test('on error code 11001', (next)=> {
            let stub = Sinon.stub(articleService, 'create', (options, callback) => {
                return callback({code: 11001});
            });
            response = Sinon.spy(() => {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.forbidden('id exists already'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.create(request, response);
        });

    });
    suite('update', ()=> {
        let request, response;
        beforeEach((next)=> {
            request = {
                payload: {
                    _id: 'test'
                },
                params: {
                    _id: 'test'
                }
            };
            return next();
        });
        test('on success', (next)=> {
            let stub = Sinon.stub(articleService, 'findByIdAndUpdate', (_id, options, callback)  => {
                return callback(null, {});
            });
            let stub2 = Sinon.stub(articleService, 'findById', (options, callback)  => {
                return callback(null, {});
            });
            response = Sinon.spy(() => {
                expect(response.called).to.be.true();
                stub.restore();
                stub2.restore();
                next();
            });
            articleController.update(request, response);
        });
        test('on error', (next)=> {
            let stub = Sinon.stub(articleService, 'findByIdAndUpdate', (_id, options, callback) => {
                return callback('error');
            });
            response = Sinon.spy(() => {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.badImplementation('error'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.update(request, response);
        });
        test('on not found', (next)=> {
            let stub = Sinon.stub(articleService, 'findByIdAndUpdate', (_id, options, callback) => {
                return callback(null, null);
            });
            let stub2 = Sinon.stub(articleService, 'findById', (options, callback) => {
                return callback(null, null);
            });
            response = Sinon.spy(() => {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.notFound('not found'))).to.be.true();
                stub.restore();
                stub2.restore();
                next();
            });
            articleController.update(request, response);
        });

    });

    suite('remove', ()=> {
        let request, response;
        beforeEach((next)=> {
            request = {
                params: {
                    _id: 'test'
                }
            };
            return next();
        });
        test('on success', (next)=> {
            let stub = Sinon.stub(articleService, 'findByIdAndRemove', (options, callback) => {
                return callback(null);
            });
            let codeSpy = Sinon.spy(() => {
                expect(response.called).to.be.true();
                expect(codeSpy.called).to.be.true();
                expect(codeSpy.calledWith(204)).to.be.true();
                stub.restore();
                next();
            });
            response = Sinon.spy(() => {
                return {
                    code: codeSpy
                };
            });
            articleController.remove(request, response);
        });
        test('on error', (next)=> {
            let stub = Sinon.stub(articleService, 'findByIdAndRemove', (options, callback) => {
                return callback('error');
            });
            response = Sinon.spy(() => {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.badRequest('error'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.remove(request, response);
        });
    });
});
