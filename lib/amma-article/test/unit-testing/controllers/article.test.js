var Hapi = require('hapi');
var Code = require('code');
var Lab = require('lab');
var Sinon = require('sinon');
var ArticleController = require('../../../controllers/article');
var ArticleService = require('../../../services/article');
var Boom = require('boom');
var lab = exports.lab = Lab.script(), before = lab.before, beforeEach = lab.beforeEach, afterEach = lab.afterEach, after = lab.after, expect = Code.expect, suite = lab.suite, test = lab.test;
suite('Test Article Controller', function () {
    var server;
    var articleService;
    var articleController;
    before(function (next) {
        server = new Hapi.Server();
        articleService = new ArticleService.default(server);
        articleController = new ArticleController.default(server);
        server.plugins = {
            'amma-article': {
                articleService: articleService
            }
        };
        next();
    });
    test('get service', function (next) {
        var service = articleController.getService();
        expect(service).to.equal(articleService);
        return next();
    });
    suite('get all', function () {
        test('on success', function (next) {
            var request = Sinon.spy();
            var response = Sinon.spy();
            var stub = Sinon.stub(articleService, 'findAll', function (options, callback) {
                return callback(null, {});
            });
            articleController.getAll(request, response);
            expect(response.called).to.be.true();
            stub.restore();
            return next();
        });
        test('on failure', function (next) {
            var request = Sinon.spy();
            var response = Sinon.spy();
            var stub = Sinon.stub(articleService, 'findAll', function (options, callback) {
                return callback('error', {});
            });
            articleController.getAll(request, response);
            expect(response.called).to.be.true();
            expect(response.calledWith(Boom.badImplementation('error'))).to.be.true();
            stub.restore();
            return next();
        });
    });
    suite('get', function () {
        var request, response;
        beforeEach(function (next) {
            request = {
                params: {
                    id: 'test'
                }
            };
            response = Sinon.spy();
            return next();
        });
        test('on success', function (next) {
            var stub = Sinon.stub(articleService, 'findById', function (options, callback) {
                return callback(null, {});
            });
            articleController.get(request, response);
            expect(response.called).to.be.true();
            stub.restore();
            return next();
        });
        test('on not found', function (next) {
            var stub = Sinon.stub(articleService, 'findById', function (options, callback) {
                return callback(null, null);
            });
            articleController.get(request, response);
            expect(response.called).to.be.true();
            expect(response.calledWith(Boom.notFound('not found'))).to.be.true();
            stub.restore();
            return next();
        });
        test('on failure', function (next) {
            var stub = Sinon.stub(articleService, 'findById', function (options, callback) {
                return callback('error', {});
            });
            articleController.get(request, response);
            expect(response.called).to.be.true();
            expect(response.calledWith(Boom.badImplementation('error'))).to.be.true();
            stub.restore();
            return next();
        });
    });
    suite('create', function () {
        var request, response;
        beforeEach(function (next) {
            request = {
                payload: {
                    _id: 'test'
                }
            };
            return next();
        });
        test('on success', function (next) {
            var stub = Sinon.stub(articleService, 'create', function (options, callback) {
                return callback(null, {});
            });
            var stub2 = Sinon.stub(articleService, 'findById', function (options, callback) {
                return callback(null, {});
            });
            var createdSpy = Sinon.spy(function () {
                expect(response.called).to.be.true();
                expect(createdSpy.called).to.be.true();
                stub.restore();
                stub2.restore();
                next();
            });
            response = Sinon.spy(function () {
                return {
                    created: createdSpy
                };
            });
            articleController.create(request, response);
        });
        test('on forbidden error', function (next) {
            var stub = Sinon.stub(articleService, 'create', function (options, callback) {
                return callback('forbidden');
            });
            response = Sinon.spy(function () {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.forbidden('forbidden'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.create(request, response);
        });
        test('on error code 11000', function (next) {
            var stub = Sinon.stub(articleService, 'create', function (options, callback) {
                return callback({ code: 11000 });
            });
            response = Sinon.spy(function () {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.forbidden('id exists already'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.create(request, response);
        });
        test('on error code 11001', function (next) {
            var stub = Sinon.stub(articleService, 'create', function (options, callback) {
                return callback({ code: 11001 });
            });
            response = Sinon.spy(function () {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.forbidden('id exists already'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.create(request, response);
        });
    });
    suite('update', function () {
        var request, response;
        beforeEach(function (next) {
            request = {
                payload: {
                    _id: 'test'
                },
                params: {
                    _id: 'test'
                }
            };
            return next();
        });
        test('on success', function (next) {
            var stub = Sinon.stub(articleService, 'findByIdAndUpdate', function (_id, options, callback) {
                return callback(null, {});
            });
            var stub2 = Sinon.stub(articleService, 'findById', function (options, callback) {
                return callback(null, {});
            });
            response = Sinon.spy(function () {
                expect(response.called).to.be.true();
                stub.restore();
                stub2.restore();
                next();
            });
            articleController.update(request, response);
        });
        test('on error', function (next) {
            var stub = Sinon.stub(articleService, 'findByIdAndUpdate', function (_id, options, callback) {
                return callback('error');
            });
            response = Sinon.spy(function () {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.badImplementation('error'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.update(request, response);
        });
        test('on not found', function (next) {
            var stub = Sinon.stub(articleService, 'findByIdAndUpdate', function (_id, options, callback) {
                return callback(null, null);
            });
            var stub2 = Sinon.stub(articleService, 'findById', function (options, callback) {
                return callback(null, null);
            });
            response = Sinon.spy(function () {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.notFound('not found'))).to.be.true();
                stub.restore();
                stub2.restore();
                next();
            });
            articleController.update(request, response);
        });
    });
    suite('remove', function () {
        var request, response;
        beforeEach(function (next) {
            request = {
                params: {
                    _id: 'test'
                }
            };
            return next();
        });
        test('on success', function (next) {
            var stub = Sinon.stub(articleService, 'findByIdAndRemove', function (options, callback) {
                return callback(null);
            });
            var codeSpy = Sinon.spy(function () {
                expect(response.called).to.be.true();
                expect(codeSpy.called).to.be.true();
                expect(codeSpy.calledWith(204)).to.be.true();
                stub.restore();
                next();
            });
            response = Sinon.spy(function () {
                return {
                    code: codeSpy
                };
            });
            articleController.remove(request, response);
        });
        test('on error', function (next) {
            var stub = Sinon.stub(articleService, 'findByIdAndRemove', function (options, callback) {
                return callback('error');
            });
            response = Sinon.spy(function () {
                expect(response.called).to.be.true();
                expect(response.calledWith(Boom.badRequest('error'))).to.be.true();
                stub.restore();
                next();
            });
            articleController.remove(request, response);
        });
    });
});
