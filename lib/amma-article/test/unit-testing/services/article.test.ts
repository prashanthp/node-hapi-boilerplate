import Hapi = require('hapi');
import Code = require('code');
import Lab = require('lab');
import Sinon = require('sinon');
import Article = require('../../../services/article');
import Mongoose = require('mongoose');

let lab = exports.lab = Lab.script(),
  before = lab.before,
  beforeEach = lab.beforeEach,
  afterEach = lab.afterEach,
  after = lab.after,
  expect = Code.expect,
  suite = lab.suite,
  test = lab.test;

suite('Test Article Service', () => {
  let server;
  let articleService;
  before((next) => {
    server = new Hapi.Server();
    articleService = new Article.default(server);
    next();
  });
  test('find all', (next) => {
    let spy = Sinon.spy();
    let query = {
      exec: spy
    };
    let stub = Sinon.stub(articleService.model, 'find', () => {
      return query;
    });
    articleService.findAll();
    stub.restore();
    expect(spy.called).to.be.true();
    return next();
  });
  test('find by id', (next) => {
    let spy = Sinon.spy();
    let query = {
      exec: spy
    };
    let stub = Sinon.stub(articleService.model, 'findById', () => {
      return query;
    });
    articleService.findById('test');
    stub.restore();
    expect(spy.called).to.be.true();
    return next();
  });

  test('create', (next) => {
    let spy = Sinon.spy();
    let stub = Sinon.stub(articleService.model, 'create').callsArgWith(1, null, {});
    articleService.create({}, spy);
    stub.restore();
    expect(spy.called).to.be.true();
    return next();
  });

  test('find by id and update', (next) => {
    let stub = Sinon.stub(articleService.model, 'findByIdAndUpdate').callsArgWith(3, null, {})
    let spy = Sinon.spy();
    articleService.findByIdAndUpdate('test', {}, spy);
    stub.restore();
    expect(spy.called).to.be.true();
    return next();
  });
  test('find by id and remove', (next) => {
    let stub = Sinon.stub(articleService.model, 'findByIdAndRemove').callsArgWith(1, null, {})
    let spy = Sinon.spy();
    articleService.findByIdAndRemove('test', spy);
    stub.restore();
    expect(spy.called).to.be.true();
    return next();
  });
});
