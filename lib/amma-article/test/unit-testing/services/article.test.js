var Hapi = require('hapi');
var Code = require('code');
var Lab = require('lab');
var Sinon = require('sinon');
var Article = require('../../../services/article');
var lab = exports.lab = Lab.script(), before = lab.before, beforeEach = lab.beforeEach, afterEach = lab.afterEach, after = lab.after, expect = Code.expect, suite = lab.suite, test = lab.test;
suite('Test Article Service', function () {
    var server;
    var articleService;
    before(function (next) {
        server = new Hapi.Server();
        articleService = new Article.default(server);
        next();
    });
    test('find all', function (next) {
        var spy = Sinon.spy();
        var query = {
            exec: spy
        };
        var stub = Sinon.stub(articleService.model, 'find', function () {
            return query;
        });
        articleService.findAll();
        stub.restore();
        expect(spy.called).to.be.true();
        return next();
    });
    test('find by id', function (next) {
        var spy = Sinon.spy();
        var query = {
            exec: spy
        };
        var stub = Sinon.stub(articleService.model, 'findById', function () {
            return query;
        });
        articleService.findById('test');
        stub.restore();
        expect(spy.called).to.be.true();
        return next();
    });
    test('create', function (next) {
        var spy = Sinon.spy();
        var stub = Sinon.stub(articleService.model, 'create').callsArgWith(1, null, {});
        articleService.create({}, spy);
        stub.restore();
        expect(spy.called).to.be.true();
        return next();
    });
    test('find by id and update', function (next) {
        var stub = Sinon.stub(articleService.model, 'findByIdAndUpdate').callsArgWith(3, null, {});
        var spy = Sinon.spy();
        articleService.findByIdAndUpdate('test', {}, spy);
        stub.restore();
        expect(spy.called).to.be.true();
        return next();
    });
    test('find by id and remove', function (next) {
        var stub = Sinon.stub(articleService.model, 'findByIdAndRemove').callsArgWith(1, null, {});
        var spy = Sinon.spy();
        articleService.findByIdAndRemove('test', spy);
        stub.restore();
        expect(spy.called).to.be.true();
        return next();
    });
});
