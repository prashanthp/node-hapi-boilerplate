import Mongoose = require("mongoose");

export interface IArticleDocument extends Mongoose.Document {
  title: string;
}

let options = {
  collection: 'article',
  schema: {
    _id: {
      type: String,
      require: true
    },
    title: {
      type: String,
      require: true
    }
  }
};
export default options;
