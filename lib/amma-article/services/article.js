var Mongoose = require("mongoose");
var ArticleSchema = require('../article-schema');
var Article = (function () {
    function Article(_server) {
        this._server = _server;
        if (!Article._model) {
            Article._schema = new Mongoose.Schema(ArticleSchema.default.schema);
            Article._model = Mongoose.model(ArticleSchema.default.collection, Article._schema);
        }
    }
    Object.defineProperty(Article.prototype, "model", {
        get: function () {
            return Article._model;
        },
        enumerable: true,
        configurable: true
    });
    Article.prototype.findAll = function (filters, next) {
        this.model.find(filters).exec(next);
    };
    Article.prototype.findById = function (id, next) {
        this.model.findById(id).exec(next);
    };
    Article.prototype.create = function (payload, next) {
        this.model.create(payload, next);
    };
    Article.prototype.findByIdAndUpdate = function (id, payload, next) {
        this.model.findByIdAndUpdate(id, payload, { upsert: false }, next);
    };
    Article.prototype.findByIdAndRemove = function (id, next) {
        this.model.findByIdAndRemove(id, next);
    };
    return Article;
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Article;
