import Hapi = require("hapi");
import Mongoose = require("mongoose");
import ArticleSchema = require('../article-schema');

class Article {
  public static _model: Mongoose.Model<ArticleSchema.IArticleDocument>;
  public static _schema: Mongoose.Schema;

  constructor(private _server: Hapi.Server) {
    if (!Article._model) {
      Article._schema = new Mongoose.Schema(ArticleSchema.default.schema);
      Article._model = Mongoose.model<ArticleSchema.IArticleDocument>(ArticleSchema.default.collection, Article._schema);
    }
  }

  get model(): Mongoose.Model<ArticleSchema.IArticleDocument> {
    return Article._model;
  }

  findAll(filters: Object, next: (err?: any, results?: ArticleSchema.IArticleDocument[]) => any): void {
    this.model.find(filters).exec(next);
  }

  findById(id: string, next: (err?: any, result?: ArticleSchema.IArticleDocument) => any): void {
    this.model.findById(id).exec(next);
  }

  create(payload: ArticleSchema.IArticleDocument, next: (err?: any, results?: any) => any): void {
    this.model.create(payload, next);
  }

  findByIdAndUpdate(id: string, payload: ArticleSchema.IArticleDocument, next: (err?: any, results?: any) => any): void {
    this.model.findByIdAndUpdate(id, payload, { upsert: false }, next);
  }

  findByIdAndRemove(id: string, next: (err?: any, results?: any) => any): void {
    this.model.findByIdAndRemove(id, next);
  }

}
export default Article;
