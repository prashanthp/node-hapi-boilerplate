var Plugin = require('../amma-plugin-loader/index');
var pkg = require('./package.json');
var PluginLoader = Plugin.default;
var config = {
    services: {
        'articleService': require('./services/article').default,
        'articleController': require('./controllers/article').default
    },
    routes: require('./routes').default,
    attributes: {
        pkg: pkg
    },
    runs: []
};
var plugin = new PluginLoader(pkg.name, config);
module.exports = plugin;
