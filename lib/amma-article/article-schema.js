var options = {
    collection: 'article',
    schema: {
        _id: {
            type: String,
            require: true
        },
        title: {
            type: String,
            require: true
        }
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = options;
