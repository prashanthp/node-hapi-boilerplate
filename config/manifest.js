var Config = require('config');
var GoodConsole = require('good-console');
var manifest = {
  server: {
    debug: {
      request: ['error']
    },
    connections: {
      routes: {
        cors: Config.cors
      }
    }
  },
  connections: [{
    port: Config.port,
    host: Config.host
  }],
  plugins: {
    'good': {
      reporters: [{
        reporter: GoodConsole,
        events: {
          request: '*',
          error: '*',
          log: '*'
        }
      }]
    },
    'halacious': null,
    './lib/amma-db': {
      options: {
        db: Config.db
      }
    },
    './lib/amma-article': null
  }
};
module.exports = manifest;
