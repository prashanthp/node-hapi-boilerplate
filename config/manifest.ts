import Glue = require('glue');
import Config = require('config');
import GoodConsole = require('good-console');

var manifest = {
  server: {
    debug: {
      request: ['error']
    },
    connections: {
      routes: {
        cors: Config.cors
      }
    }
  },
  connections: [{
    port: Config.port,
    host: Config.host
  }],
  plugins: {
    'good': {
      reporters: [{
        reporter: GoodConsole,
        events: {
          request: '*',
          error: '*',
          log: '*'
        }
      }]
    },
    'halacious': null
  }
};
export = manifest;
