Aim
--------------------------
This project aims to provide a most efficient and effective boilerplate using hapi.js and typescript.

Installation
-----------------------------
1. Clone the repository
2. Navigate to project home directory. run the following command to install the node modules
   npm install
3. Run the application using the following command
   node index.js
4. Check config/default.json for configuration related information. In production you can have config/production.json which overrides the default.json

Running the Testcases
------------------------
To run the unit test cases use the following command  
gulp unit:test

To run the unit test cases with the coverage report use the following command  
gulp test:unit:coverage

To install typings using the following command  
gulp tsd:install

Highlights
----------------------------
1. 100% unit test coverage
2. module based approach - for example conside article plugin in lib, which includes all the functionality exclusively related to articles(routes,validation, services, dbcollection, schema etc )
3. modules can be turned on or off - check config/manifest.ts
4. Typescript - makes developer life easy by providing code hints and suggests the developer to follow standards
5. hapi.js - A rich framework for building applications and services - for more info check http://hapijs.com/
6. Validation - server side validation
7. Most useful server related information when the server is turned on.

Usage
----------------------
1. Get All articles  
   - GET - http://localhost:3000/articles
2. Get an article  
   - http://localhost:3000/articles/article1
3. create an article  
   - Post - http://localhost:3000/articles
   - form-data: {_id: article1, title: article name}
4. Update an article  
   - Put - http://localhost:3000/articles/article1
   - form-data: {title: article name}  
5. Delete an article  
   - delete - http://localhost:3000/articles/article1

Note: I am using an editor, which automatically do converts the typescript code into JavaScript. if you are not able to, use a gulp watcher to do so.
